
#include "StateMachine.h"
#include "StateMachineException.hpp"


template <typename event_type>
StateMachine<event_type>::StateMachine(std::string oStateName):
// Set the statemachine as a "machine" state
State<event_type>(oStateName, nullptr){}

template <typename event_type>
State<event_type>* StateMachine<event_type>::poGetState(std::string oStateName, bool bAllowNonExistance){
    // Check if the state is the statemachine itself
    if(oStateName == ((State<event_type>*)this)->m_oStateName){
        return this;
    }
    // Cycle through all the statemachines' states
    for(State<event_type>* poState : oStatesVector){
        // Find appropriate state
        if(oStateName == poState->m_oStateName){
            return poState;
        }
    }

    if(false == bAllowNonExistance){
	    throw StateMachineException("STATEMACHINE FAILED. Cannot find \""+oStateName+"\" state");
    }

    return nullptr;
}

template <typename event_type>
void StateMachine<event_type>::vAddState(std::string oStateName, fptr pfHandler){
    // Do not accept duplicates
    for(State<event_type>* poState : oStatesVector){
        if(oStateName == poState->m_oStateName){
	        throw StateMachineException("STATEMACHINE FAILED. Cannot accept duplicate state \"" + oStateName + "\"");
        }
    }
    oStatesVector.push_back(new State<event_type>(oStateName, pfHandler));
}

template <typename event_type>
void StateMachine<event_type>::vAddState(State<event_type>* poNewState){
    // Do not accept duplicates
    for(State<event_type>* poState : oStatesVector){
        if(poNewState->m_oStateName == poState->m_oStateName){
	        throw StateMachineException("STATEMACHINE FAILED. Cannot accept duplicate state \"" + poNewState->m_oStateName + "\"");
        }
    }
    oStatesVector.push_back(poNewState);
}

template <typename event_type>
void StateMachine<event_type>::vStartState(std::string oStateName){
    m_poCurrentState = poGetState(oStateName);
    m_poStartState = m_poCurrentState;
}

template <typename event_type>
void StateMachine<event_type>::vEndState(std::string oStateName){
    m_poEndState = poGetState(oStateName);
}



template <typename event_type>
void StateMachine<event_type>::vAddTransition(State<event_type>* poNextState, event_type oTriggeringEvent){
    m_poEndState->vAddTransition(poNextState, oTriggeringEvent);
}

template <typename event_type>
void StateMachine<event_type>::vAddTransition(std::string oStartStateName, std::string oEndStateName, event_type oTriggeringEvent){
    State<event_type>* poStartState = poGetState(oStartStateName);
    State<event_type>* poEndState = poGetState(oEndStateName);

    // Connect last statemachine state to the state at the end of the transition
    poStartState->vAddTransition(poEndState, oTriggeringEvent);
}

template <typename event_type>
void StateMachine<event_type>::vAddTransitionDefault(std::string oStartStateName, std::string oEndStateName){
    State<event_type>* first = poGetState(oStartStateName);
    State<event_type>* second = poGetState(oEndStateName);

    first->m_poDefaultTransitionState = second;
}

template <typename event_type>
void StateMachine<event_type>::vAddTransitionImmediate(std::string oStartStateName, std::string oEndStateName){
    State<event_type>* first = poGetState(oStartStateName);
    State<event_type>* second = poGetState(oEndStateName);

    first->m_poImmediateTransitionState = second;
}



template <typename event_type>
void StateMachine<event_type>::vReactImmediately(){
    // State has an immediate transition
    if(nullptr != m_poCurrentState->m_poImmediateTransitionState){
        // Set new state
        m_poCurrentState = m_poCurrentState->m_poImmediateTransitionState;
        // Re-trigger handler
        m_poCurrentState->vAct();
        // Recursively check for more imediate transitions
        vReactImmediately();
    }
}


template <typename event_type>
State<event_type>* StateMachine<event_type>::poReactEvent(event_type oNewEvent, State<event_type>* poNextState){

    // We are not in an imediate transition
    if(nullptr == poNextState){
        // Make the current state react (launches next states' handler)
        poNextState = m_poCurrentState->poReactEvent(oNewEvent);
        
        // There is no next state (no reaction) stay on this statemachine
        if(nullptr == poNextState){
        // Nothing else to do
            return nullptr;
        }
    }
    
    // The next state is this statemachine
    if(this == poNextState){
        m_poCurrentState = m_poStartState;
    }else{
        m_poCurrentState = poNextState;
    }

    // Next state isn't inside this statemachine
    if(nullptr == poGetState(m_poCurrentState->m_oStateName, true)){
        // Exit this statemachine (restart it)
        m_poCurrentState = m_poStartState;
        // Return next state
        return poNextState;
    }

    // Still on this statemachine, must run handler
    m_poCurrentState->vAct();

    // There is an immediate transition
    if(nullptr != m_poCurrentState->m_poImmediateTransitionState){
        return poReactEvent(oNewEvent, m_poCurrentState->m_poImmediateTransitionState);
    }

    // Nothing else to do
    return nullptr;
}

template <typename event_type>
State<event_type>* StateMachine<event_type>::poReactEvent(event_type oNewEvent){
    return poReactEvent(oNewEvent, nullptr);
}

template <typename event_type>
void StateMachine<event_type>::vStart(){
    m_poStartState->vAct();
}

template <typename event_type>
void StateMachine<event_type>::vAct(){
    m_poCurrentState->vAct();
}
