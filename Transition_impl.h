
#include "Transition.h"

template <typename event_type>
Transition<event_type>::Transition(
    State<event_type>* poCurrentState,
    State<event_type>* poNextState,
    event_type oTargetEvent):

m_poCurrentState(poCurrentState),
m_poNextState(poNextState),
m_oTargetEvent(oTargetEvent)

{}

template <typename event_type>
bool Transition<event_type>::bEvaluate(event_type oEventId){
    // Compare incoming event with triggering event
    return oEventId == m_oTargetEvent;
}
