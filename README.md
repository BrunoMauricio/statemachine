# StateMachine

Proof of concept implementation of a general purpose state machine object in C++

## Concept
The main concepts in the statemachine are the states that make it up and the events that trigger the transitions between states.

States are simple function handlers.

Transition are triggered by the comparison between a received event, and the registered ones.

As such, the "transition event types" just need to support the "==" operator, so it can be a user defined class or any integral type.

To interact with the machine, simply make it react to an event, and according to the current states, it might perform a transition.

The statemachine IS NOT multi threaded, and each state handler runs until the end before the machine proceeds (this may change in the future).

Statemachines have a start, current and end state.

As Statemachines inherit the State class, they can be nested inside each other (as long as the event types match). A statemachine "state" starts with its' "start state" and ends when its' "end state" is reached.

A state can hold as many "normal" transitions as necessary. There are two types of "special" transitions, and a state can have only one of each at most.

"default transitions" are trigerred when the event that is being evaluated does not trigger any other transition. It is the "if nothing triggers, trigger this" transition.

"immediate transitions" are trigerred immediately after a handler runs and need to event to be triggered. Analogous to "empty" graph transitions.

If it is possible for multiple transitions to be triggered on a given event, the one actually triggered is the one that was first inserted.

To create sensible statemachines, states, transitions and events is 100% up to you :)

## Usage
For concrete implementation examples, see the classes in the ExampleStateMachine.h/cpp

To create a statemachine, first import StateMachine.h

Second implement the statemachine your heart desires.

And thirdly, make the statemachine react to whatever events come up.

Statemachine implementation template (ironic):
```
enum transition_type{
    t_some_event
};

class STATEMACHINE_NAME : public StateMachine<transition_type>{
    // Here, define the state handlers (states can have no handler as well)
    // Handlers are called when a state becomes active
    static void firstStateHandler(){
        // do something
    }
    static void secondStateHandler(){
        // do something
    }
    
    public:
    // Define the main statemachine body
    STATEMACHINE_NAME():
        // Give a name to the statemachine (only necessary for nested statemachines, but always required)
        StateMachine("Some statemachine identifier")
    {
        // Define states by their name and handler (handler is nullptr if omitted)
        // vAddState("name", handler);
        // From each respective addState forwards, states are identified by their given names
        vAddState("FirstStateName", firstStateHandler);
        vAddState("SecondStateName", secondStateHandler);

        // Define start and end states for this statemachine
        vStartState("FirstStateName");
        vEndState("SecondStateName");

        // Define all transitions
        // vAddTransition("from states' name", "to states' name, triggering_transition)
        vAddTransition("SubSubStart", "SubSubEnd", t_some_event);
    }

};

// to use the state machine, first instantiate the machine in question
STATEMACHINE_NAME machine;
// Then start it
machine.start();
// Now, for each transition event, make the machine react
machine.machineReact(transition);

//To create a statemachine state, instead of the "normal" addState method, call

//vAddState(new ADifferentStateMachine("That statemachines' name"));

```


## Roadmap
* Main priority
* 1) Base sanity checkers:
* * Make statemachines (and all their nested states and statemachines) perform base sanity checks on start:
* * * Check if all statemachines have a start and an end state
* 2) Improve state handler:
* * Allow outside handlers or even non static handlers;
* * Allow passing basic information (perhaps the event?) to the handler.
* 3) Multi-threading:
* * Launch all state handlers in a separate thread and have the statemachine wait for them to end;
* * Queue incoming events.
* 4) Multi-threading configuration:
* * Allow state-based configuration for statemachine behavior when running a handler in a separate thread (wait for handler to finish? timeout after x seconds?);
* * Allow statemachine configuration for handling event priority queue when waiting for a handler to finish.
* Secondary priority
* * Add graphviz support to output the state machine diagrams of a state machine
* * Perform mathematical analysis of the statemachines (like preventing infinite loops via immediate/empty transitions, or adding an automatic way to check for unreachable states? idk, could be fun)

## Contributing
This is a proof of concept, and perhaps even useless, but I'm open to suggestions.

## Authors and acknowledgment
Authored by Bruno Mauricio (@BrunoMauricio)

## License
This project is not meant/allowed to be used anywhere. This may change in the future, on request.

## Project status
On going

