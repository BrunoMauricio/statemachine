#ifndef STATEMACHINE_STATEMACHINE
#define STATEMACHINE_STATEMACHINE

#include <vector>

#include "State.h"

/**
 * StateMachine template class
 */
template <class event_type>
class StateMachine : public State<event_type> {
    private:

    // The internal state machines' states
    std::vector<State<event_type>*> oStatesVector;

    // Returns a particular state
    /**
     *  Searches for a particular state
     *  @param oStateName State name to search for
     *  @param bAllowNonExistance Whether the function should thrown an exception if the state isn't found
     *  @return A pointer to the found state, or nullptr if no state was found (plus bAllowNonExistance == true)
     */
    State<event_type>* poGetState(std::string oStateName, bool bAllowNonExistance=false);

    protected:

    State<event_type>* m_poCurrentState = nullptr;
    State<event_type>* m_poStartState = nullptr;
    State<event_type>* m_poEndState = nullptr;
    
    /**
     *  Constructor. Sets up the state
     *  @param oStateName Statemachine state name and identifier
     *  @return The statemachine object, ready to rock
     */
    StateMachine(std::string oStateName);

    /**
     *  Creates and adds a new state to the state machine
     *  @param oStateName State name and identifier
     *  @param pfHandler State handler
     */
    void vAddState(std::string oStateName, fptr pfHandler);

    /**
     *  Adds a new state to the state machine
     *  @param oStateName State name and identifier
     *  @param pfHandler State handler
     */
    void vAddState(State<event_type>* poNewState);

    /**
     *  Set the start state via its' name. It must have already been added via addState
     *  @param oStateName Name of state to set.
     */
    void vStartState(std::string oStateName);
    
    /**
     *  Set the end state via its' name. It must have already been added via addState
     *  @param oStateName Name of state to set.
     */
    void vEndState(std::string oStateName);

    /**
     *  Adds a transition between two states.
     *  @param oStartStateName Name of the first state for the transition
     *  @param oEndStateName Name of the second state for the transition
     *  @param oTriggeringEvent The event to trigger the transition between the two states
     */
    void vAddTransition(std::string oStartStateName, std::string oEndStateName, event_type oTriggeringEvent);

    /**
     *  Adds a default transition from one state to another.
     *  @param oStartStateName Name of the first state for the transition
     *  @param oEndStateName Name of the second state for the transition
     */
    void vAddTransitionDefault(std::string oStartStateName, std::string oEndStateName);

    /**
     *  Add an immediate transition, from one state to another.
     *  @param oStartStateName Name of the first state for the transition
     *  @param oEndStateName Name of the second state for the transition
     */
    void vAddTransitionImmediate(std::string oStartStateName, std::string oEndStateName);

    /**
     *  Handles immediate transition related recursive calls.
     */
    void vReactImmediately();

    /**
     *  Perform Statemachine action (launch current_state handler).
     */
    void vAct() override;

    public:

    /**
     *  Start statemachine.
     */
    void vStart();

    /**
     *  Setup a new transition starting at this statemachine (aka the end state), with the given trigger event
     *  @param poNextState State name and identifier
     *  @param oTriggeringEvent The event that triggers the transition
     */
    void vAddTransition(State<event_type>* poNextState, event_type oTriggeringEvent) override;


    /**
     *  Calls poReactEvent with a nullptr second argument.
     *  Used to redirect the override of the State poReactEvent into an appropriate statemachine method
     *  @param oNewEvent Event to react to
     *  @return The return value of the poReactEvent(oNewEvent, nullptr) call
     */
    State<event_type>* poReactEvent(event_type oNewEvent) override;

    /**
     *  Make the statemachine react to an event. Where most of the statemachine logic resides.
     *  The working principle is the following:
     *  A states' poReactEvent simply returns the next state, or nullptr in which case nothing further is required
     *  If the next state is a normal state, its' handler is expected to run
     *  If the next state is a statemachine, that statemachine must react appropriately
     *  For imediate transitions, the second argument must be the transitioned into state and not nullptr
     *  @param oNewEvent Event to react to
     *  @param poNextState If not null, it is the next imediately transitioned into state to evaluate
     *  @return > nullptr if it isn't necessary for a superior stateamchine to do anything else (the state handler has run)
     *          > pointer to a state in case the current statemachine transitions out of itself (and the next states' handler hasn't run yet)
     */
    State<event_type>* poReactEvent(event_type oNewEvent, State<event_type>* poNextState);

};

#include "StateMachine_impl.h"

#endif