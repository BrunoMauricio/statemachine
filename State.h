#ifndef STATEMACHINE_STATE
#define STATEMACHINE_STATE

#include <vector>
#include <string>

#include "Transition.h"

// The handler type
typedef void (*fptr)(void);

/**
 * State template class (mostly data structure)
 * Represents a possible transition between two events
 * Are directional (from state x to state y) and registered on state x
 */
template <typename event_type>
class State{
    public:
    // This states' outgoing transitions
    std::vector<Transition<event_type>> m_oTransitionsVector;
    
    // This states' name (and unique identifier)
    std::string m_oStateName;

    // The handler this state calls
    fptr m_pfHandler;

    // If different from nullptr, holds the target state transition
    // for all events not matching other transitions
    State* m_poDefaultTransitionState = nullptr;

    // If different from nullptr, holds the target state transition
    // that happens immediately after calling this states' handler
    State* m_poImmediateTransitionState = nullptr;

    /**
     *  Constructor. Sets up the state
     *  @param oStateName State name and identifier
     *  @param pfHandler Handler that runs when this state is reached
     *  @return The state object, ready to rock
     */
    State(std::string oStateName, fptr pfHandler);

    /**
     *  Setup a new transition starting at this state, with the given trigger event
     *  @param poNextState State name and identifier
     *  @param oTriggeringEvent The event that triggers the transition
     */
    virtual void vAddTransition(State<event_type>* poNextState, event_type oTriggeringEvent);

    /**
     *  Makes the state react to the given event
     *  @param oNewEvent Event to react to
     *  @return The "next" state after reacting to the received event. Could be this state, or a "transitioned into" one
     */
    virtual State<event_type>* poReactEvent(event_type oNewEvent);

    /**
     *  Perform State action (launch handler)
     */
    virtual void vAct();
};

#include "State_impl.h"

#endif