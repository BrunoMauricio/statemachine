#include "ExampleStateMachine.h"
#include "StateMachineException.hpp"


void testMachines(){
    ExampleSuperStateMachine machine;
	// NULL -> START (handler)
    machine.vStart();

	// START -> SUB1
	// NULL -> SUBSTART (handler)
	machine.poReactEvent(t_StartToMiddle);
	
	// ...
	// SUBSTART -> SUBSUB
	// NULL -> SUBSUBSTART (handler)
	machine.poReactEvent(t_sub_simple_transition);

	// ...
	// ...
	// SUBSUBSTART -> SUBSUBSTEND (handler)
	machine.poReactEvent(t_sub_sub_simple_transition);
	
	// ...
	// ...
	// SUBSUBSTEND -> SUBSUBSTART (handler)
	machine.poReactEvent(t_sub_loop_transition);
	
	// ...
	// ...
	// SUBSUBSTART -> SUBSUBSTEND (handler)
	machine.poReactEvent(t_sub_sub_simple_transition);

	// ...
	// SUBSUB -> SUBEND (handler)
	// SUBSUBSTEND -> NULL
	machine.poReactEvent(t_sub_simple_transition);

	// SUB1 -> MIDDLE (handler)
	// SUBEND -> NULL
	machine.poReactEvent(t_Middle1to2);
	
	// MIDDLE -> SUB2 (handler)
	// NULL -> SUBSTART (handler)
	machine.poReactEvent(t_Middle1to2);

	// ...
	// SUBSTART -> SUBSUB
	// NULL -> SUBSUBSTART (handler)
	machine.poReactEvent(t_sub_simple_transition);

	// ...
	// ...
	// SUBSUBSTART -> SUBSUBSTEND (handler)
	machine.poReactEvent(t_sub_sub_simple_transition);

	// ...
	// SUBSUB -> SUBEND (handler)
	// SUBSUBSTEND -> NULL
	machine.poReactEvent(t_sub_simple_transition);
	
	// SUB2 -> END (handler)
	// SUBEND -> NULL
	machine.poReactEvent(t_MiddleToEnd);

	// END -> DEFAULT -> IMMEDIATE -> END
	machine.poReactEvent(t_MiddleToEnd);


}

int main(int argc, char* argv[]){
	testMachines();
}