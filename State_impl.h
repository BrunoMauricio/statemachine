
#include "State.h"

template <typename event_type>
State<event_type>::State(std::string oStateName, fptr pfHandler):
m_oStateName(oStateName), m_pfHandler(pfHandler)
{}

template <typename event_type>
void State<event_type>::vAddTransition(State<event_type>* poNextState, event_type oTargetEvent){
    m_oTransitionsVector.push_back(Transition(this, poNextState, oTargetEvent));
}

template <typename event_type>
State<event_type>* State<event_type>::poReactEvent(event_type oNewEvent){
    // Cycle through transitions, see which triggers first
    for(Transition<event_type>& transition : m_oTransitionsVector){
        if(transition.bEvaluate(oNewEvent)){
            // Return next state
            return transition.m_poNextState;
        }
    }
    // No transition was triggered, is there a default transition?
    if(nullptr != m_poDefaultTransitionState){
        return m_poDefaultTransitionState;
    }

    return nullptr;
}

template <typename event_type>
void State<event_type>::vAct(){
    if(nullptr != m_pfHandler){
        m_pfHandler();
    }
}