#ifndef STATEMACHINE_TRANSITION
#define STATEMACHINE_TRANSITION

#include <cstdint>
#include <string>

template <typename event_type>
class State;

/**
 * Transition template class (mostly data structure)
 * Represents a possible transition between two events
 * Are directional (from state x to state y) and registered on state x
 */
template <typename event_type>
class Transition{
    // Protect this class, only State should interact with Transition
    friend class State<event_type>;

    // The event that triggers this transition
    event_type m_oTargetEvent;

    // The state to transition into
    State<event_type>* m_poNextState;
    // The state to transition from
    State<event_type>* m_poCurrentState;

    /**
     *  Constructor. Sets up the transition
     *  @param poCurrentState The state to transition from
     *  @param poNextState The state to transition into
     *  @param oTargetEvent The event that triggers this transition
     *  @return The transition object, ready to evaluate events
     */
    Transition(State<event_type>* poCurrentState, State<event_type>* poNextState, event_type oTargetEvent);

    /**
     *  Evaluates the transition condition between the two states using the equality operator
     *  @param oEventId The event to evaluate
     *  @return Returns the comparison between the received event and 
     */
    bool bEvaluate(event_type oEventId);
};

#include "Transition_impl.h"

#endif