#ifndef STATEMACHINE_EXAMPLE
#define STATEMACHINE_EXAMPLE

#include "StateMachine.h"


enum Supertransitions{
    t_StartToMiddle,
    t_MiddleToEnd,
    t_sub_simple_transition,
    t_sub_loop_transition,
    t_sub_sub_simple_transition,
    t_Middle1to2
};

class ExampleSubSubStateMachine : public StateMachine<Supertransitions>{
    static void SubStart(){
        printf("SUBSUBSTART STATE\n");
    }
    static void SubEnd(){
        printf("SUBSUBEND STATE\n");
    }

    public:
    ExampleSubSubStateMachine(std::string state_name):
        StateMachine(state_name){
        vAddState("SubSubStart", SubStart);
        vAddState("SubSubEnd", SubEnd);

        vStartState("SubSubStart");
        vEndState("SubSubEnd");

        vAddTransition("SubSubStart", "SubSubEnd", t_sub_sub_simple_transition);
    }
};

class ExampleSubStateMachine : public StateMachine<Supertransitions>{
    static void SubStart(){
        printf("SUBSTART STATE\n");
    }
    static void SubEnd(){
        printf("SUBEND STATE\n");
    }

    public:
    ExampleSubStateMachine(std::string state_name):
        StateMachine(state_name){

        vAddState("SubStart", SubStart);
        vAddState(new ExampleSubSubStateMachine("SubSub"));
        vAddState("SubEnd", SubEnd);

        vStartState("SubStart");
        vEndState("SubEnd");

        vAddTransition("SubStart", "SubSub", t_sub_simple_transition);
        vAddTransition("SubSub", "SubSub", t_sub_loop_transition);
        vAddTransition("SubSub", "SubEnd", t_sub_simple_transition);
    }
};

class ExampleSuperStateMachine : public StateMachine<Supertransitions>{
    // Define each State handlers
    static void Start(){
        printf("START STATE\n");
    }
    static void End(){
        printf("END STATE\n");
    }
    static void Middle(){
        printf("MIDDLE STATE\n");
    }
    static void Default(){
        printf("DEFAULT STATE\n");
    }
    static void Immediate(){
        printf("IMMEDIATE STATE\n");
    }

    public:
    ExampleSuperStateMachine():
        StateMachine("Example State Machine"){
        // Define states (first state defined is the starting state)
        vAddState("Start", Start);
        vAddState("Middle", Middle);
        vAddState("End", End);
        vAddState(new ExampleSubStateMachine("Sub1"));
        vAddState(new ExampleSubStateMachine("Sub2"));

        // Add special states
        vAddState("Default", Default);
        vAddState("Immediate", Immediate);

        // Define starting state
        vStartState("Start");
        vEndState("End");

        // Define all transitions
        vAddTransition("Start", "Sub1", t_StartToMiddle);
        vAddTransition("Sub1", "Middle", t_Middle1to2);
        vAddTransition("Middle", "Sub2", t_Middle1to2);
        vAddTransition("Sub2", "End", t_MiddleToEnd);

        vAddTransitionDefault("End", "Default");
        vAddTransitionImmediate("Default", "Immediate");
        vAddTransitionImmediate("Immediate", "End");
    }
};


/*
Run this function to see the machine working
*/
void testMachines();

#endif