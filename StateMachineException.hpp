#ifndef STATEMACHINE_EXCEPTION
#define STATEMACHINE_EXCEPTION

#include <exception>
#include <string>

struct StateMachineException : public std::exception
{
    std::string m_oExceptionReason;
    StateMachineException(std::string oExceptionReason): m_oExceptionReason(oExceptionReason){}

	const char * what () const throw ()
    {
    	return m_oExceptionReason.c_str();
    }
};

#endif